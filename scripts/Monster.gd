extends KinematicBody2D

export var _speed = 0
var _direction = Vector2(0,0)
var _target = null
var _chase_mode = load('res://scripts/IChasable.gd').new(self)

func _physics_process(delta):
	_chase_mode.chase()
	move_and_slide(_direction * _speed)

func distance_to_target():
	return position.distance_to(_target.position)

func get_speed():
	return _speed

func set_speed(speed):
	_speed = speed

func get_position():
	return position

func set_position(pos):
	position = pos

func get_direction():
	return _direction

func set_direction(direction):
	_direction = direction

func get_target():
	return _target

func set_target(target):
	_target = target

func get_chase_mode():
	return _chase_mode

func set_chase_mode(chase_mode):
	_chase_mode = chase_mode
