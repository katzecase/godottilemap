extends Node2D

enum ActionState {OFF, TRIGGER1}
var _action_state = ActionState.OFF #: ActionState
var _tile: Vector2

func _ready():
	$Walls/Monster.set_target($Walls/Player)
	$Walls/Monster.set_chase_mode(load('res://scripts/ScentTrail.gd').new($Walls/Monster))

func _process(_delta):
	if Input.get_action_strength("ui_accept")\
	 && $TimerActionBreak.is_stopped():
		$TimerActionBreak.start()
		match _action_state:
			ActionState.OFF:
				pass
			ActionState.TRIGGER1:
				get_node("Walls").switch_door_status(_tile)
				pass

func add_scent(scent):
	add_child(scent)

# trigger area 1 for doors
func OnTriggerArea1Entered(_body, tile):
	_action_state = ActionState.TRIGGER1
	_tile = tile

func OnTriggerArea1Exited(_body):
	_action_state = ActionState.OFF

func get_action_state():
	return _action_state

func get_timer_action_break():
	return $TimerActionBreak

func set_action_state(state):
	_action_state = state
