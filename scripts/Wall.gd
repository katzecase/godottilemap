extends TileMap

onready var _dungeon = get_parent()
const door_trigger_offset = Vector2(0,25)
var _doors = []
var _trigger_areas: Array

func _ready():
	_doors = get_used_cells_by_id(4) + get_used_cells_by_id(5)\
		+ get_used_cells_by_id(7) + get_used_cells_by_id(8)
	build_doors()

func build_doors():
	for tile_coordiate in _doors:
		build_door(tile_coordiate, get_cellv(tile_coordiate))
	for trigger in _trigger_areas:
		trigger.connect("body_entered", _dungeon, "OnTriggerArea1Entered", [trigger.get_tile()])
		trigger.connect("body_exited", _dungeon, "OnTriggerArea1Exited")

func build_door(tile_pos: Vector2, index_of_door):
	set_cellv(tile_pos, index_of_door)
	# add trigger for door
	var t_area = preload("res://scenes/TriggerArea1.tscn").instance()
	# place slighty below to fit tilemap view
	var world_pos = map_to_world(tile_pos) + door_trigger_offset
	t_area._setup(world_pos, tile_pos, index_of_door)

	_trigger_areas.append(t_area)
	add_child(t_area)

func switch_door_status(tile_loc: Vector2):
	var index = get_cellv(tile_loc)
	if(index == 4):
		set_cellv(tile_loc, 5)
	elif(index == 5):
		set_cellv(tile_loc, 4)
	elif(index == 7):
		set_cellv(tile_loc, 8)
	elif(index == 8):
		set_cellv(tile_loc, 7)

func get_trigger_areas():
	return _trigger_areas

func set_trigger_areas(trigger_areas):
	_trigger_areas = trigger_areas

func add_scent(scent):
	if get_parent().has_method('add_scent'):
		get_parent().add_scent(scent)

func get_doors():
	return _doors

func set_doors(doors):
	_doors = doors
