extends IChasable

func _init(chaser).(chaser):
	_chaser = chaser

func chase():
	var look = _chaser.get_node("RayCast2D")
	look.cast_to = (_chaser.get_target().get_position()\
					- _chaser.get_position())
	look.force_raycast_update()

	
	for scent in _chaser.get_target().get_scent_trail():
		if !look.is_colliding():
			_chaser.set_direction(look.cast_to.normalized())
			break
		
		look.cast_to = (scent.get_position() - _chaser.get_position())
		look.force_raycast_update()
