extends Area2D

var _tile
var _tile_region

# pos			collision position
# tile			tile location to set tile
# tile_region	tile index
func _setup(pos, tile, tile_region):
	position = pos
	_tile = tile
	_tile_region = tile_region

func get_tile():
	return _tile
