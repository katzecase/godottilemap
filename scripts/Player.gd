extends KinematicBody2D

const Scent = preload("res://scenes/Scent.tscn")

export var _speed = 300
var velocity = Vector2()
var scent_trail = []

func _ready():
	$ScentTimer.connect("timeout", self, "add_scent")

func _physics_process(_delta):
	get_input()
	move_and_slide(velocity)

func get_input():
	velocity = Vector2()
	if Input.is_action_pressed('move_right'):
		velocity.x += 1
	if Input.is_action_pressed('move_left'):
		velocity.x -= 1
	if Input.is_action_pressed('move_down'):
		velocity.y += 1
	if Input.is_action_pressed('move_up'):
		velocity.y -= 1
	velocity.y /= 2
	velocity = velocity.normalized() * _speed

func add_scent():
	var scent = Scent.instance()
	set_scent(scent)

func set_scent(scent):
	scent._setup(self, position)
	scent_trail.push_front(scent)
	if get_parent().has_method('add_scent'):
		get_parent().add_scent(scent)

func set_position(pos):
	position = pos

func get_position():
	return position

func set_speed(speed):
	_speed = speed

func get_speed():
	return _speed

func get_scent_trail():
	return scent_trail
