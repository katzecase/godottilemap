extends Node2D

var _player

func _setup(player, pos):
	_player = player
	position = pos

func _ready():
	$Timer.connect("timeout", self, "remove_scent")

func remove_scent():
	_player.get_scent_trail().erase(self)
	queue_free()

func get_position():
	return position
