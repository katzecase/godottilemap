extends 'res://addons/gut/test.gd'

var Monster = load('res://scripts/Monster.gd')
var Player = load('res://scripts/Player.gd')


func test_can_make_Monster():
	assert_not_null(autofree(Monster.new()))

func test_get_set_speed():
	var m = autofree(Monster.new())
	assert_accessors(m, 'speed', 0, 200)

func test_get_set_target():
	var m = autofree(Monster.new())
	var player = autofree(Player.new())

	assert_accessors(m, 'target', null, player)

func test_get_set_chase_mode():
	var m = autofree(Monster.new())
	var Ichasable = load('res://scripts/IChasable.gd')
	var scent_trail = load('res://scripts/ScentTrail.gd')
	assert_is(m.get_chase_mode(), Ichasable)
	m.set_chase_mode(scent_trail.new(m))
	assert_is(m.get_chase_mode(), scent_trail)
