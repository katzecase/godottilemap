extends 'res://addons/gut/test.gd'

var Wall = load('res://scripts/Wall.gd')
var TriggerArea1 = load('res://scenes/TriggerArea1.tscn')

func test_can_make_wall():
	assert_not_null(autofree(Wall.new()))

func test_get_set_trigger_areas():
	var w = autofree(Wall.new())
	var t = autofree(preload('res://scenes/TriggerArea1.tscn').instance())
	var pos = Vector2(500, 500)
	var tile = Vector2(2, 5)
	t._setup(pos, tile, 4)
	
	assert_accessors(w, 'trigger_areas', [], [t])

func test_get_set_doors():
	var w = autofree(Wall.new())
	assert_accessors(w, 'doors', [], Vector2(5, 1))

func test_build_door_at_certain_tile_locations():
	var w = autofree(Wall.new())
	var tile_locations = [Vector2(0, 0), Vector2(0, 2), Vector2(1, 7), Vector2(3, 5)]
	for i in range(tile_locations.size()):
		tile_locations[i] = tile_locations[i]
		w.build_door(tile_locations[i], 4)

	var door_tile_locations: Array
	for dl in w.get_trigger_areas():
		door_tile_locations.append(dl.get_tile())

	assert_eq(door_tile_locations, tile_locations)

func test_door_can_function():
	var w = autofree(Wall.new())
	var tile_loc = Vector2(5, 5)
	w.build_door(tile_loc, 4)

	w.switch_door_status(tile_loc)
	assert_eq(w.get_cellv(tile_loc), 5)

	w.switch_door_status(tile_loc)
	assert_eq(w.get_cellv(tile_loc), 4)

func test_door_at_other_direction_can_function():
	var w = autofree(Wall.new())
	var tile_loc = Vector2(7, 20)
	w.build_door(tile_loc, 7)

	w.switch_door_status(tile_loc)
	assert_eq(w.get_cellv(tile_loc), 8)

	w.switch_door_status(tile_loc)
	assert_eq(w.get_cellv(tile_loc), 7)
