extends "res://addons/gut/test.gd"

var Player = load('res://scripts/Player.gd')
var PlayerScene = load('res://scenes/Player.tscn')
var Scent = load('res://scenes/Scent.tscn')

func test_can_make_player():
	assert_not_null(autofree(Player.new()))

func test_get_set_speed():
	var p = autofree(Player.new())
	assert_accessors(p, 'speed', 300, 100)

func test_player_can_add_and_remove_scent():
	var player = autofree(PlayerScene.instance())
	var pos = Vector2(0,0)
	player.set_position(pos)
	add_child_autofree(player)

	var scent = autofree(Scent.instance())
	player.set_scent(scent)
	assert_eq(player.get_scent_trail(), [scent], 'add 1 scent')

	scent.remove_scent()
	assert_eq(player.get_scent_trail(), [], '0 scent left')

# test inputs
func test_player_can_move_up():
	var player = add_child_autofree(PlayerScene.instance())
	player.set_position(Vector2(0,0))
	player.set_speed(10)
	var input = autofree(InputEventAction.new())
	input.action = "move_up"
	input.pressed = true
	Input.parse_input_event(input)

	simulate(player, 1, 1)

	assert_lt(player.position.y, 0.0)
