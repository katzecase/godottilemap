extends 'res://addons/gut/test.gd'

var Dungeon = load('res://scenes/Dungeon.tscn')
var _dungeon = null
var DungeonScript = load('res://scripts/Dungeon.gd')
var Wall = load('res://scripts/Wall.gd')
var ScentTrail = load('res://scripts/ScentTrail.gd')

var Scent = load('res://scenes/Scent.tscn')

func before_each():
	_dungeon = autofree(Dungeon.instance())
	add_child(_dungeon)

func after_each():
	remove_child(_dungeon)

func test_can_create_Dungeon():
	assert_not_null(autofree(Dungeon.instance()))

func test_trigger_area_1_can_enter_and_exit():
	var wall = _dungeon.get_node("Walls")
	var door_loc = Vector2(2, 5)
	wall.build_door(door_loc, 4)
	var map = wall.map_to_world(door_loc)

	var player = wall.get_node("Player")
	_dungeon.OnTriggerArea1Entered(player, door_loc)
	assert_eq(_dungeon.get_action_state(), _dungeon.ActionState.TRIGGER1, 'player enterd, should be trigger1(1)')

	_dungeon.OnTriggerArea1Exited(player)
	assert_eq(_dungeon.get_action_state(), _dungeon.ActionState.OFF, 'player exited, should be off(0)')

func test_player_can_open_and_close_door_4():
	var wall = _dungeon.get_node("Walls")
	var door_loc = Vector2(2, 5)
	wall.build_door(door_loc, 4)
	var map = wall.map_to_world(door_loc)

	var player = wall.get_node("Player")
	_dungeon.OnTriggerArea1Entered(player, door_loc)

	_dungeon.get_timer_action_break().set_wait_time(.2)

	var input = InputEventAction.new()
	input.action = "ui_accept"
	input.pressed = true
	Input.parse_input_event(input)
	simulate(_dungeon, 5, 1)
	assert_eq(wall.get_cellv(door_loc), 5)

	yield(yield_for(.1), YIELD)
	Input.parse_input_event(input)
	assert_eq(wall.get_cellv(door_loc), 5, 'action break, door should be still close')

	yield(yield_for(.2), YIELD)
	Input.parse_input_event(input)
	assert_eq(wall.get_cellv(door_loc), 4, 'action break finished, door should be still open')

func test_player_can_open_and_close_door_7():
	var wall = _dungeon.get_node("Walls")
	var door_loc = Vector2(2, 5)
	wall.build_door(door_loc, 7)
	var map = wall.map_to_world(door_loc)

	var player = wall.get_node("Player")
	_dungeon.OnTriggerArea1Entered(player, door_loc)

	var input = InputEventAction.new()
	input.action = "ui_accept"
	input.pressed = true

	_dungeon.get_timer_action_break().set_wait_time(.2)
	Input.parse_input_event(input)
	simulate(_dungeon, 1, 1)
	assert_eq(wall.get_cellv(door_loc), 8)

	yield(yield_for(.1), YIELD)
	Input.parse_input_event(input)
	assert_eq(wall.get_cellv(door_loc), 8, 'action break, door should be still close')

	yield(yield_for(.2), YIELD)
	Input.parse_input_event(input)
	assert_eq(wall.get_cellv(door_loc), 7, 'action break finished, door should be still open')

func test_trigger_area_1_is_connected():
	var dungeon = autofree(Dungeon.instance())
	add_child_autofree(dungeon)
	var wall = autofree(Wall.new())
	replace_node(dungeon, 'Walls', wall)
	wall.set_doors([Vector2(9, 2)])
	wall.build_doors()


	for trigger in wall.get_trigger_areas():
		assert_connected(trigger, dungeon, 'body_entered', 'OnTriggerArea1Entered')
		assert_connected(trigger, dungeon, 'body_exited', 'OnTriggerArea1Exited')

func test_player_scent_is_add_to_dungeon():
	var player = _dungeon.get_node("Walls").get_node("Player")
	var pos = Vector2(0,0)
	player.set_position(pos)

	var scent = autofree(Scent.instance())
	player.set_scent(scent)
	assert_true(_dungeon.has_node(scent.get_path()), 'add 1 scent')

func test_monster_can_chase_player():
	var monster = _dungeon.get_node("Walls").get_node("Monster")
	monster.set_speed(10)
	print(monster.get_position())

	var player = _dungeon.get_node("Walls").get_node("Player")
	var distance_orig = monster.distance_to_target()
	simulate(_dungeon, 10, 1)
	assert_lt(monster.distance_to_target(), distance_orig, "I'll catch you...")
