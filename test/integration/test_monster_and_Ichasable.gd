extends "res://addons/gut/test.gd"

var Monster = load('res://scripts/Monster.gd')
var IChasable = load('res://scripts/IChasable.gd')
var ScentTrail = load('res://scripts/ScentTrail.gd')

var InMonster = load('res://scenes/Monster.tscn')
var Player = load('res://scenes/Player.tscn')

func test_can_no_chase():
	var chaser = autofree(Monster.new())
	chaser.set_target(autofree(Node2D.new()))
	add_child_autofree(chaser)
	
	var distance_orig = chaser.distance_to_target()

	simulate(chaser, 1, 1)
	assert_eq(chaser.distance_to_target(), distance_orig, "not chase yet.")

	chaser.set_direction(Vector2(1, 1))
	chaser.set_speed(5)
	simulate(chaser, 1, 1)
	assert_ne(chaser.distance_to_target(),  distance_orig, """chase without any chasing mode,
			  speed: 5, dir: (1,1), delta: 1""")

func test_scent_trail_can_chase_straight_forward():
	var player = Player.instance()
	add_child_autofree(player)

	var chaser = InMonster.instance()
	chaser.set_target(player)
	chaser.set_speed(200)
	add_child_autofree(chaser)
	var scent_trail = autofree(ScentTrail.new(chaser))
	chaser.set_chase_mode(scent_trail)

	var distance_orig = chaser.distance_to_target()
	simulate(chaser, 1, 1)
	var distance_after = chaser.distance_to_target()
	assert_lt(distance_after, distance_orig, \
			  "I am getting closer...")

	simulate(chaser, 5, .1)
	assert_lt(chaser.distance_to_target(), distance_after, \
			  "I am getting even closer...")

