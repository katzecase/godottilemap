extends 'res://addons/gut/test.gd'

var Dungeon = load('res://scenes/Dungeon.tscn')

func test_player_can_add_scent_to_dungeon():
	var dungeon = autofree(Dungeon.instance())
	var player = dungeon.get_node("Walls").get_node("Player")
	var pos = Vector2(0,0)
	player.set_position(pos)
	player.add_scent()

	assert_true(true)
